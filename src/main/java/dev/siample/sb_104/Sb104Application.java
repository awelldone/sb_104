package dev.siample.sb_104;

import java.util.Arrays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;

// @Configuration <- @Bean
// @EnableAutoConfiguration
// @ComponentScan
@SpringBootApplication
public class Sb104Application {

	@Bean
	public Person giveAPerson() {
		return new Person("Tim", 39);
	}
	
	public static void main(String[] args) {
		ApplicationContext aContext = SpringApplication.run(Sb104Application.class, args);
		
		String[] beanArray = aContext.getBeanDefinitionNames();
		Arrays.sort(beanArray);
		
		for (String beanName : beanArray) {
			System.out.println(beanName);
		}
	}

}
