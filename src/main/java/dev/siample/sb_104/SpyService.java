package dev.siample.sb_104;

import org.springframework.stereotype.Service;

@Service("spying")
public class SpyService {
	
	public String iSaySg() {
		return "sb_104: my beannames: giveAPerson, spying.";
	}
	
}
